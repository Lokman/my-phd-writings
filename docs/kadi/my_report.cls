\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{my_report}[2017/02/23 My basic internal reports class]
\LoadClass{article}

%% Page size and margins
%%%\usepackage{geometry}
%%%\geometry{
%%%  a4paper,
%%%  total={170mm,257mm},
%%%  left=20mm,
%%%  top=20mm,
%%%}
\usepackage[margin=30mm]{geometry}


%% Use URW Palladio L font
\usepackage{amstext}
\usepackage{mathpazo}
%%%\usepackage[sc]{mathpazo}
\linespread{1.20} % Palladio needs more leading (space between lines)
\usepackage[T1]{fontenc}

%% For clickable references and urls
\usepackage{hyperref}

%% For inline enumeration
\usepackage[inline]{enumitem}



%% Written following tutorial https://www.sharelatex.com/blog/2011/03/27/how-to-write-a-latex-class-file-and-design-your-own-cv.html
%% Checkout this link also http://tex.stackexchange.com/questions/35854/creating-a-common-settings-file
