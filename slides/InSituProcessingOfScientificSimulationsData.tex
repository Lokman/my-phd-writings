%% beamer tutorial http://web.mit.edu/rsi/www/pdfs/beamer-tutorial.pdf

\documentclass[pdf]{beamer}
\mode<presentation>{}

\usepackage{textcomp} %% for right arrow in text mode
\usepackage{tcolorbox} %% for important messages
\usepackage{graphicx} %% to include figure and images

%% TOFIX rgb color not working very well
\definecolor{olivegreen}{rgb}{0,1,0}

%% set a theme from pre-definded ones
%\mode<presentation>{\usetheme{Warsaw}}

%\setbeamercolor{background canvas}{bg=black}
%\setbeamercolor{normal text}{fg=white}

%% preambule
\title{In Situ Processing of Scientific Simulations' Data}
\subtitle{history, motivation, opportunities and challenges}
\author{Lokman Rahmani}

\begin{document}

%% title frame
\begin{frame}
  \titlepage
\end{frame}

%\section{Introduction}

%% normal frame
\begin{frame}{Abstract}
  \pause
  Optimizing the time-to-solution for HPC experimentations requires more than 
  optimizing the simulation itself. Indeed, current HPC scientific 
  experimentations group many analysis tasks applied to the generated data to 
  extract useful knowledge from it. Tasks coupling consists of connecting those 
  analysis tasks with the simulation and with each others. In situ processing 
  presents a promising solution for running HPC scientific experimentations 
  efficiently and effectively
\end{frame}

\begin{frame}{Goals}
\begin{itemize}
  \item<2-> Understand the concept of in situ processing in the context of 
  scientific simulation experiments,
  \item<3-> its history,
  \item<4-> its motivations (the need for it),
  \item<5-> its opportunities and chanllenges,
  \item<6-> existing in situ processing solutions
\end{itemize}
\end{frame}

\begin{frame}{Terminology - Definitions}
\begin{itemize}
  \item<2-> Scientific simulations
  \begin{itemize}
    \item Solves hard mathematical equations that describe real life 
    phenomena~\cite{?}
  \end{itemize}
  \item<3-> Scientific simulations' data
  \begin{itemize}
    \item Multi-variant multi-dimensional time-varying data
    \item Distributed over processors/cores
  \end{itemize}
  \item<4-> Scientific simulations' data processing
  \begin{itemize}
    \item The simulation is ONLY the first step
    \item Scientific Questions \textrightarrow Scientific Discovery
    \item Scientific discovery through advanced computing
  \end{itemize}
  %% TOFIX show on full page
  \only<5-5>{
  \begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{figs/computational-scientific-process.png}
  \end{figure}
  }
  \item<6-> In situ processing
  \begin{itemize}
    \item In situ: Latin for 'on site'
    \item Process the data on the same machine, as the simulation runs
  %% TOFIX show on full page
  \only<7-7>{
  \begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{figs/computational-scientific-process-in-situ.png}
  \end{figure}
  }
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Data Processing (Traditional) Techniques}
\begin{itemize}
  \item Post-Processing
  \begin{itemize}
    \item Simulation's output is simply dumped to a mass-storage system and 
    studied at later time
    \item Processing is mainly done on the scientist desktop
  \end{itemize}
  \item Co-Processing
  \begin{itemize}
    \item Simulation's output is directly transfered to a processing 
    (visualization mostly) machine for immediate processing
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Motivation for In Situ Processing}
\begin{itemize}
  \item As scientists gain access to more powerful machines they try to solve 
  larger, more complex problems
  \begin{itemize}
    \item More and more data is generated
  \end{itemize}
  \item However, I/O capabilities are not growing at the same rate
  \begin{itemize}
    \item Highly impact the simulation performance
    \item Cannot transfer all the data to viz cluster - {\color{red} 
    Co-processing}
    \item Cannot transfer all the data to scientists desktop - {\color{red}
    Post-processing}
  \end{itemize}
  \item Naive Solution, store
  \begin{itemize}
    \item Less data – reduce data resolution
    \item Less often data – skip complete time steps
  \end{itemize}
  \pause
% \begin{tcolorbox}[colback={blue},outer arc=0mm]
  \colorbox{red}{
  Defeats the purpose of running high precision simulations
  }
% \end{tcolorbox}
\end{itemize}
\end{frame}

\begin{frame}{Motivation for In Situ Processing Bis}
\begin{itemize}
  \item Software Productivity - Platforms Profitability
  \pause
  \begin{itemize} 
    \item ``A key component for overall scientific productivity'' \\
    Identified as one of the top ten exa-scale research challenges~\cite{?}
    \item ``workflow \& analysis productivity'' \& ``the value of computational 
    output'' \\
    part of the eight software productivity challenge~\cite{?}
  \end{itemize}
\end{itemize}
\pause
\begin{figure}
  \centering
  \includegraphics[width=0.3\textwidth]{figs/top-ten-exa-challenges-cover.png}
  \hspace{1cm}
  %% TOFIX add "2014" text between the two figures
  \includegraphics[width=0.3\textwidth]{figs/software-productivity-for-extreme-scale-cover.png}
\end{figure}
\end{frame}

\begin{frame}{The In Situ Proposition}
\begin{itemize}
  \item In situ processing
  \begin{itemize}
    \item Reduce or Process the data on the same machine as the simulation runs
    \item Minimize data requiring storage or transfer
  \end{itemize}
  \item This opens for more promising opportunities
\end{itemize}
\end{frame}

\begin{frame}{In Situ Processing Opportunities}
\begin{itemize}
  \item Avoid I/O jitter – don't block the simulation - {\color{green}Tech}
  \item Avoid bandwidth extreme/heavy usage  - {\color{green}Tech}
  \item Allow interactive steering - ``Close the loop'' - {\color{green}Tech+Sci}
  \item Study the full-extent of the data - extract physical-based features of 
  interest  - {\color{green}Sci}
  \item Benefits all the subsequent data analysis and movement  - 
  {\color{green}Tech+Sci}
  \item Foster scientific software productivity – shift the focus on the 
  time-to-solution rather than the solver time - {\color{green}Tech+Sci}
  \item Provides monitoring and metric about the simulation run and data - 
  {\color{green}Tech+Sci}
\end{itemize}
\pause
%% TOFIX set text color, use floating box
\colorbox{green}{ With in situ processing, if done carefully utilizing domain 
knowledge, There is no need to keep the full raw data }
\end{frame}

\begin{frame}{Analysis Tasks That Can Run In Situ}
\begin{itemize}
  \item<2-> \color<3->[rgb]{0,0,0.6}Data reduction
  \only<3-3>{
  \begin{itemize}
    \item Sub-sampling: in either spacial or temporal dimension
    \item Quantization: reduce precision of data floating points values
    \item Transform-based compression
    \begin{itemize}
      \item Commonly used in block-based data
      \item Local multi-resolution data depending on subsequent analysis and 
      available resources
    \end{itemize}
  \end{itemize}
  }
  \item<2-> \color<4->[rgb]{0,0,0.6}Feature extraction
  \only<4-4>{
  \begin{itemize}
    \item Scientific simulation running boils down to identifying feature in data
    \item Scientists do not always know exactly what they are looking for
    \item Can be considered as a high-level data reduction using domain knowledge
  \end{itemize}
  }
  \item<2-> \color<5->[rgb]{0,0,0.6}Quality assessement
  \only<5-5>{
  \begin{itemize}
    \item The corresponding information loss must be conveyed to the user of 
    reduced dataset
    \begin{itemize}
    \item Full-reference method \textrightarrow not possible
    \item Reduced-reference method \textrightarrow promising
    \end{itemize}
  \end{itemize}
  }
  \item<2-> \color<6->[rgb]{0,0,0.6}Visualization
  \only<6-6>{
  \begin{itemize}
    \item Very  important analysis task, as it is very suitable for humans
    \item Most effective for interactive steering
    \item Consists of two steps
    \begin{enumerate}
      \item rendering - local
      \item composting - global
    \end{enumerate}
  \end{itemize}
  }
\end{itemize}
\uncover<8->{
%% TOFIX set text color, use floating box
\colorbox{green}{ Almost all analysis tasks of scientific simulations  data can 
be run in situ}
}
\end{frame}

\begin{frame}{In Situ Processing Challenges}
\begin{itemize}
  \item Scientists are reluctant to use their computational power to run 
  analysis (mostly visualization)
  \begin{itemize}
    \item {\color{red} Not any more}
    \item More hardware accelerators are integrated in supercomputers
  \end{itemize}
  \item No standard API for data management and data visualization
  \item Efficiency
  \begin{itemize}
    \item Zero-copy memory - share the data between the simulation and analysis 
    tasks 
    \item Simulation domain decomposition don't fit the need for processing 
    tasks, especially for visualization - load-balancing issues
  \end{itemize}
  \item Analysis tasks deployement
  \begin{itemize}
    \item How to co-run simulation and analysis tasks efficiently
  \end{itemize}
  \item Fault-tolerance
  \begin{itemize}
    \item Very important as the data will reside in volatile memory 
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{In Situ Processing Challenges}
%% TOFIX how to draw with latex
\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{figs/challenges-venn-diagram.png}
\end{figure}
\end{frame}

\begin{frame}{In Situ, Wayback Machine}
\begin{itemize}
  \item First started as steering framework (\~1990-1995)
  \begin{itemize}
    \item Not interactive, mostly
    \item Multiple frameworks were developed:~\cite{?}
  \end{itemize}
  \item Enable interactivity with simulation (\~1995-2000)
  \begin{itemize}
    \item Simulation-time visualization
    \item very tightly-coupled – developed with the simulation
  \end{itemize}
  \item Co-processing (\~2000-2005)
  \begin{itemize}
    \item Transfer the data to visualization machine equipped with hardware 
    accelerators
    \item Until petascale reached: lots of data to transfer
  \end{itemize}
  \item In Situ as we know it (since 2009)
  \begin{itemize}
    \item Solutions based on I/O frameworks~\cite{?}
    \item Solutions based on Dataflows/workflow frameworks~\cite{?}
  \end{itemize}
  \item Scientific visualization software evolution
  \begin{itemize}
    \item >1990 Tcl/tk \textrightarrow 1993 VTK \textrightarrow 2000 VisIt 
    \textrightarrow 2003 ParaView
    \item \~2007 In situ support – Libsim (VisIt) – ParaView Co-Processing (Catalyst recently)
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{The Dream In Situ Framework}
\begin{itemize}
  \item<3-> {\color{blue} Ultimate: The MPI-like standard for tasks coupling 
  in HPC platforms}
  \uncover<4->{
  \item Data management API
  \begin{itemize}
    \item Both for syntax  and semantics description
    \item Data identification
    \item Data identification
    \item Maps to I/O file format (HDF5, netCDF, \dots)
  \end{itemize}
  \item Domain decomposition
  \begin{itemize}
    \item Flexible domain decomposition
    \item Used for all analysis tasks
    \item Efficient collective communication
  \end{itemize}
  \item Dataflow platform for tasks coupling
  \item Full-package
  \begin{itemize}
    \item On demand data storage
    \item On demand checkpointing
    \item On demand quality assessment
  \end{itemize}
  \item Steering
  \begin{itemize}
    \item Interactive reproducible steering
  \end{itemize}
  \item Fault-tolerance
  \begin{itemize}
    \item In memory checkpointing
  \end{itemize}
  }
\end{itemize}
\end{frame}

\begin{frame}{Conclusion - In Situ Insights}
\begin{itemize}
  %\color{}
  \item The slow growing of I/O and bandwidth capabilities pushed forward to:
  \begin{itemize}
    \color{olivegreen}
    \item Reborn scientific-important capabilities, like interactive steering
    \item Focus on the end-to-end performance not the solver only
    \item Scientific simulation experiments productivity
    \item Make different HPC specialists to work even closer
  \end{itemize}
\end{itemize}
\end{frame}

\end{document}
